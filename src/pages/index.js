import React from 'react';
import { Typography } from '@material-ui/core';
import Layout from '../components/Layout';

const IndexPage = () => (
  <Layout>
    <Typography variant="h1">Avenir Font</Typography>
  </Layout>
);

export default IndexPage;
